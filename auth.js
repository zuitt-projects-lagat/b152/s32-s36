/*
	We will create our own module which will have methods that will help us authorize our users to access or to disallow access to certain parts/features in our app.
*/

//imports
const jwt = require("jsonwebtoken");
const secret = "CourseBookingApi";

/*
	JWT is a way to securely pass information from one part of a server to the frontend or other parts of our application. This will allow us to authorize our users to access or disallow access to certain parts of our app.

	JWT is like a gift wrapping serving able to encode our user details which can only be unwrapped by jwt's own methods and if the secret provided is intact.

	IF the jwt seemed tampered with we will reject the users attempt to access a feature in our app.
*/

module.exports.createAccessToken = (user) => {

	//check if we can receive the details of the foundUser in our login:
	//console.log(user);
	//data object is created to contain some details of our user
	const data = {

		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin

	}
	//console.log(data);
	//create our jwt with data payload, our secret and the algorithm to create our JWT token

	return jwt.sign(data, secret, {});

}

module.exports.verify = (req, res, next) => {

	//middlewares which have access to req, res and next also can send responses to the client.

	//req.headers.authorization contains sensitive data especially our token/jwt
	//console.log(req.headers.authorization);
	let token = req.headers.authorization;

	//IF we are not passing token in our request authorization in our postman authorization, then here in our api, req.headers.authorization will be undefined.

	//This if statement will first check IF token variable contains undefined or a proper jwt. If it is undefined, we will check token's data type with typeof, then send a message to the cllient.
	if (typeof token === "undefined"){
		return res.send({auth: "Failed. No Token."})
	} else {

		//We will check if the token is legit before proceeding to the next middleware/controller.

		//console.log(token);//token before slice
		/*
			slice() is a method which can be used on strings and arrays.

			This will allow us to copy a part of the string.

			slice(<staringPosition>, <endPosition>)

			Starting position indicates the index number we will copy up to but not including the item in that position.

			Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxZjBjM2IzZGI2YjMyN2YzYzFiOTU5NCIsImVtYWlsIjoic3BpZGVybWFuM0BnbWFpbC5jb20iLCJpc0FkbWluIjpmYWxzZSwiaWF0IjoxNjQzMjQ1MjMwfQ.HzCQAfO3kp_PyYkY13T5CPpZKsF4V9GexHV3JwrCJhQ

			"Peter"

			slice(3, string.length)

			"er"

			Essentially, we extracted just the jwt token and re-assigned to our token variable.

		*/
		token = token.slice(7, token.length)
		//console.log(token);

		//verify the legitimacy of our token:
		jwt.verify(token, secret, function(err, decodedToken){
			//err will contain the error from decoding your token. This will contain the reason why we will reject the token.
			//IF verification of the token

			if(err){
				return res.send({
					auth: "Failed",
					message: err.message
				});
			} else {
				//console.log(decodedToken);//contains the data from our token
				//We will add a new property to the request object. In fact, anything that we add to the request object, we can still get from our next middle ware or controller.
				req.user = decodedToken;
				//user property will be added to request object and will contain our decodedToken. It can be accessed in the next middleware/controller.

				//next() will let us proceed to the next middleware OR controller
				next ();

			}
		})

	}

}

//verifyAdmin will also be used a middleware.
module.exports.verifyAdmin = (req, res, next) => {

	//verifyAdmin comes after the verify middleware,
	//Do we have any access to our user's isAdmin detail?
	//We can get details from req.user because verifyAdmin comes after verify method.
	//Note: You can only have req.userfor any middleware or controller that comes after verify.

	//console.log(req.user);

	if(req.user.isAdmin){
		//if the logged in user, based on his token is an admin, we will proceed to the next middleware/controller
		next();
	} else {
		return res.send({

			auth: "Failed",
			message: "Action Forbidden"
			
		})
	}

}