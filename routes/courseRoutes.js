const express = require("express");
const router = express.Router();

//import course controllers
const courseControllers = require("../controllers/courseControllers");

const auth = require("../auth");
const {verify, verifyAdmin} = auth;

//Create New Course (Activity 2)
//Since this route should not be accessed by all users, we have to authorize only some logged in users who have proper authority:
//First, verify if the token the user using is legit.
router.post('/', verify, verifyAdmin, courseControllers.createCourse);

//Get All Courses (Activity 2)
router.get('/', courseControllers.getAllCourses);

//Get Single Course (Activity 3)
router.get('/getSingleCourse/:id', courseControllers.getSingleCourse);

//Update Course
router.put('/:id', verify, verifyAdmin, courseControllers.updateCourse);

//Archive a Course Document (Activity 4)
router.put('/archive/:id', verify, verifyAdmin, courseControllers.archiveCourse);

//Activate a Course Document (Activity 4)
router.put('/activate/:id', verify, verifyAdmin, courseControllers.activateCourse);

//Retrieve all active courses (Activity 4)
router.get('/getActiveCourses', courseControllers.getActiveCourses);

//Retrieve all inactive courses
router.get('/getInactiveCourses', verify, verifyAdmin, courseControllers.getInactiveCourses);

//Check Course Name
router.post('/checkCourseName', courseControllers.checkCourseName);

//Check Course Price
router.post('/checkCoursePrice', courseControllers.checkCoursePrice);

//Check Course's Enrollees (Activity 5)
router.get('/getEnrollees/:id', verify, verifyAdmin, courseControllers.getEnrollees);

module.exports = router;