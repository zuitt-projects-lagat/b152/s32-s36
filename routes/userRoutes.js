const express = require("express");
const router = express.Router();

//import user controllers
const userControllers = require("../controllers/userControllers");
//userControllers is a module we imported. In JS, modules are objects.
//exported controllers are methods to their modules.
//console.log(userControllers)

//We willl import auth module so we can use our verify and verifyAdmin method as middleware for our routes.
const auth = require("../auth");

//destructure verify and verifyAdmin from auth. Auth, is an imported module, so therefore it is an object in JS.
const {verify, verifyAdmin} = auth;

//console.log(verify);

//User Registration:
router.post('/', userControllers.registerUser);

//Get All Users:
router.get('/', userControllers.getAllUsers)

//user login
router.post('/login', userControllers.loginUser);

//get logged in users details
//verify method from auth will be used as a middle ware. Middleware are functions we can use around our app. In the context of expressjs route, middleware are functions we add in the route and can receive the request, response and next objects. We can have more than 1 middleware before the controller.
//router.get('<endpoint>', middleware, controller);
//When a route has middlewares and controller, we will run through the middleware first before we get to the controller.
//Note: Routes that have verify as a middleware would require us to pass a token from postman.
router.get('/getUserDetails', verify, userControllers.getUserDetails);

//Check Email (Activity 3)
router.post('/checkEmailExists/', userControllers.checkEmail);

//Update User Details
router.put('/updateUserDetails', verify, userControllers.updateUserDetails);

//Make User Admin
router.put('/updateAdmin/:id', verify, verifyAdmin, userControllers.updateAdmin);

//Enroll a Registered User
//Because you are using the verify middleware, we are supposed to pass a token.
//Doing so, allows us to verify the legitimacy of our token AND get the details of our user from the token and save it in our req.user
router.post('/enroll', verify, userControllers.enroll);

//Receive User's Enrolled Courses (Activity 5)
router.get('/getEnrollments', verify, userControllers.getEnrollments);

module.exports = router;