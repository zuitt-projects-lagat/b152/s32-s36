//import User Model
const User = require("../models/User");

//import Course Model
const Course = require("../models/Course");

//import bcrypt
const bcrypt = require("bcrypt");
//bcrypt is a package which will help us add a layer of security for our user's passwords.

//auth module
const auth = require("../auth");
//console.log(auth);//this is our own auth js module.

module.exports.registerUser = (req,res) => {

	//Does this controller need user input?
	//yes.
	//How/Where can we get our user's input?
	//req.body

	console.log(req.body);

	/*
		bcrypt adds a new layer of security to our user's password.

		What bcrypt does is hash our password into a randomized character version of the original string.

		It is able to hide your password within that randomized string.

		Syntaax:
		bcrypt.hashSync(<stringToBeHashed>, <saltRounds>)

		Salt-Rounds are the number of times the characters in the hash are randomized

	*/

	//Create a variable to store the hashed/encrypted password. Then this new hashed pw will be what save in our database.
	const hashedPW = bcrypt.hashSync(req.body.password, 10);
	//console.log(hashedPW); //IF you want to check if the password was hashed.

	//Create a new user document out of our User model:
	let newUser = new User({

		//Check your model, check the fields you need in your new document.
		//Check your req.body if you have all the values needed for each field.
		//Add hashedPW to add the hashed password in the new document.
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: hashedPW,
		mobileNo: req.body.mobileNo

	});

	//newUser is our new document created out of our User model.
	//new documents created out of the User model have access to some methods.
	//.save() method will allow us to save our new document into the collection the that our models is connected to.
	//note: do not add a semicolon to your then() chain
	newUser.save()
	.then(user => res.send(user))
	.catch(err => res.send(err));


}

module.exports.getAllUsers = (req,res) => {

	User.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));

}

module.exports.loginUser = (req,res) => {

	//Does this controller need user input
	//yes
	//Where can we get this user input?
	//req.body
	console.log(req.body);//contains user credentials

	/*
		1. Find the user by the email.
		2. If we found a user, we will check his password.
		3. If we don't find a user, we will send a message to the client.
		4. If upon checking the found user's password is the same as our input password, we will generate "key" to access our app. If not, we will turn him away by sending a message in the client.
	*/

	User.findOne({email: req.body.email})
	.then(foundUser => {

		//console.log(foundUSer);
		if (foundUser === null){
			return res.send( "No User Found.")
		} else {
			//if we find a user, the foundUser parameter, will contain the details of the document that matched our req.body.email
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password);
			console.log(isPasswordCorrect);
			/*
				syntax:
				bcrypt.compareSync(<string>, <hashedString>)
				
				"spidermanOG"

				"$2b$10$CFtvr4sK9bMFrb.GCpNcGeHB8swFzGDf4LeWR3/fBc/hZhwitpCoq"

			*/
			if (isPasswordCorrect) {
				/*
					auth.createAccessToken will receive our foundUser as an argument. Then, in the createAccessToken() will return a "token" or "key" which contains some of our user's details.

					This key is encoded and only our own methods will be able to decode it.

					auth.createAccessToken will return a JWT token out of our user details. Then, with res.send() we will be able to send the token to our client.

					This token or key will now let us allow or disallow users to access certain parts
				*/
				return res.send({accessToken: auth.createAccessToken(foundUser)})
			} else {
				return res.send("Incorrect Password.")
			}
		}

	})
	.catch(err => res.send(err));

}

//allow us to get the details of our logged in user
module.exports.getUserDetails = (req,res) => {

	/*
		req.user contains details from our decodedToken because the route for this controller uses verify as a middleware. Routes that do not use verify will not have access to req.user.
	*/

	console.log(req.user);

	//Find our logged in user's document from our db and send it to the client by its id
	//Model.findById() will allow us to look for a document using an id
	//Model.findById() = db.collection.findOne({_id: "id"})
	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));
	//res.send("testing for verify");

}

//allow us to check if email exists (Activity 3)
module.exports.checkEmail = (req,res) => {

	//console.log(req.body.email);//check if you can receive the email from our client's request body.
	
	//You can use find({email: req.body.email}), however, find() returns multiple documents in an ARRAY.

	//SPIDERMAN3@gmail.com is not the same as spiderman3@gmail.com

	User.findOne({email: req.body.email})
	.then(result => {

		//console.log(result)

		if (result !== null && result.email === req.body.email){

			return res.send("Email is already registered!");
			
		} else {

			return res.send("Email is available.");

		}
	})
	.catch(err => res.send(err));

}

module.exports.updateUserDetails = (req,res) => {

	console.log(req.body)//input for new values for our user's details.
	console.log(req.user.id)//check the logged in user's id

	//What method can we use to update our user's details?
	//findByIdAndUpdate

	let updates = {

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		mobileNo: req.body.mobileNo

	}

	User.findByIdAndUpdate(req.user.id, updates, {new: true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err));

}

module.exports.updateAdmin = (req,res) => {

	//console.log(req.user)//WE can still have req.user because we are passing a token. HOWEVER, the details in req,user will be the admin's because you are passing the admin's token.
	console.log(req.params.id)//id of the user we want to update.

	User.findByIdAndUpdate(req.params.id, {isAdmin: true}, {new: true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err));

}

module.exports.enroll = async (req,res) => {

	//console.log("test enroll route")

	console.log(req.user.id);//the user's id from the decoded token after verify()
	console.log(req.body.courseId)//the course id from our request body

/*	User.findById(req.user.id)
	.then(EnrolledUser => {


	})*/
	if (req.user.isAdmin){
		res.send("Action Forbidden");
	}

	let isUserUpdated = await User.findById(req.user.id).then(user => {

		//check if you found the user's document
		//console.log(user);

		//Add the courseId in an object and push the object in the user's enrollments array:
		let newEnrollment = {
			courseId: req.body.courseId
		}

		//access the enrollments array

		user.enrollments.push(newEnrollment);

		//save the changes
		return user.save().then(user => true).catch(err => err.message);

	})

	//console.log(isUserUpdated);

	if (isUserUpdated !== true){
		return res.send({message: isUserUpdated})
	}

	//Find the course we will push our enrollee in
	let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {

		//console.log(course);

		let enrollee = {
			userId: req.user.id
		}

		//push the enrollee into the enrollees
		course.enrollees.push(enrollee);

		//Save the course document and our update
		//Return true as value of isCourseUpdated
		//Return the err.message as value of isCourseUpdated
		return course.save().then(course => true).catch(err => err.message);

	})

	//console.log(isCourseUpdated);

	//stop the process of there was an error saving our course document.
	if(isCourseUpdated !== true){
		return res.send({message: isCourseUpdated})
	}

	//send a message to the client that we have successfully enrolled our user if both isUserUpdated and isCourseUpdated contain the boolean true
	if(isUserUpdated && isCourseUpdated){
		return res.send({message: "Enrolled Successfully."})
	}

}

//allow us to find user's enrolled courses (Activity 5)
module.exports.getEnrollments = (req,res) => {

	//console.log(req.user.id);

	User.findById(req.user.id)
	.then(enrolledUser => res.send(enrolledUser.enrollments))
	.catch(err => res.send(err));

};