//import Course Model
const Course = require("../models/Course");

//allow us to create a new course document (Activity 2)
module.exports.createCourse = (req,res) => {

	console.log(req.body);

	//Create a new course document out of our Course model:
	let newCourse = new Course({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price

	});

	newCourse.save()
	.then(course => res.send(course))
	.catch(err => res.send(err));

}

//allow us to get all course documents (Activity 2)
module.exports.getAllCourses = (req,res) => {

	Course.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));
	
}

//allow us to retrieve single course (Activity 3)
module.exports.getSingleCourse = (req, res) => {

	console.log(req.params)

	Course.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));

}

//update course: name, description and price
module.exports.updateCourse = (req,res) => {

	//console.log();//Where can we get our course's id?
	//console,log(req.body)//contains our new values.

	//updates object will contain the field/fields to update and its new value.
	let updates = {

		name: req.body.name,
		description: req.body.description,
		price: req.body.price

	}

	//use finByIdAndUpdate() to update our course
	Course.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(err => res.send(err));

}

//allow us to archive a course (Activity 4)
module.exports.archiveCourse = (req,res) => {

	console.log(req.params);

	let archive = {

		isActive: false

	}

	Course.findByIdAndUpdate(req.params.id, archive, {new:true})
	.then(archivedCourse => res.send(archivedCourse))
	.catch(err => res.send(err));

}

//allow us to activate a course (Activity 4)
module.exports.activateCourse = (req,res) => {

console.log(req.params);

	let activate = {

		isActive: true

	}

	Course.findByIdAndUpdate(req.params.id, activate, {new:true})
	.then(activatedCourse => res.send(activatedCourse))
	.catch(err => res.send(err));

}

//allow us to retrieve active courses (Activity 4)
module.exports.getActiveCourses = (req,res) => {

	Course.find({isActive: true})
	.then(result => res.send(result))
	.catch(err => res.send(err));

}

//allow us to retrieve inactive courses
module.exports.getInactiveCourses = (req,res) => {

	Course.find({isActive: false})
	.then(result => res.send(result))
	.catch(err => res.send(err));

}

//allow us to check the course name
module.exports.checkCourseName = (req,res) => {

	//console.log(req.body);

	Course.find({name: {$regex: req.body.name, $options: '$i'}})
	.then(result => {

		if (result.length === 0){

			return res.send("No Courses Found.");
			
		} else {

			return res.send(result);

		}

	})
	.catch(err => res.send(err));

}

//allow us to check the course price
module.exports.checkCoursePrice = (req,res) => {

	//console.log(req.body);

	Course.find({price: req.body.price})
	.then(result => {

		if (result.length === 0){

			return res.send("No Courses Found.");
			
		} else {

			return res.send(result);

		}
		
	})
	.catch(err => res.send(err));

}

//allow us to see the enrollees in a course (Activity 5)
module.exports.getEnrollees = (req,res) => {

	console.log(req.params.id);

	Course.findById(req.params.id)
	.then(course => res.send(course.enrollees))
	.catch(err => res.send(err));

}