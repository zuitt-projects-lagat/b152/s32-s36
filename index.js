const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 4000;

//connect api to db:
mongoose.connect("mongodb+srv://gnikkolagat:1234512345@cluster0.klkgh.mongodb.net/bookingAPI152?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	});

//notifications
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", ()=>console.log("Connected to MongoDB"));

app.use(express.json());

//import route from userRoutes
const userRoutes = require('./routes/userRoutes');
//use our routes and group together under '/users'
app.use('/users', userRoutes);

//import route from courseRoutes
const courseRoutes = require('./routes/courseRoutes');
//use our routes and group together under '/courses'
app.use('/courses', courseRoutes);

app.listen(port, () => console.log(`Server running at localhost:4000`));